﻿CREATE TABLE [dbo].[YairDB] (
    [email] TEXT  NOT NULL,
    [isAdmin] BIT NOT NULL,
    [password]  TEXT NOT NULL,
    [username]     TEXT NOT NULL,
    PRIMARY KEY CLUSTERED ([username] ASC)
);

