﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="YairShalom_CS_FinalProject.Home" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <link rel="apple-touch-icon" sizes="72x72" href="favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
    <link rel="manifest" href="favicons/site.webmanifest">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#e32626">
    <meta name="msapplication-TileColor" content="#000000">
    <meta name="theme-color" content="#ffffff">

    
    <link rel="stylesheet" type="text/css" href="reset.css">
    <link rel="stylesheet" type="text/css" href="Home.css">
    <link rel="stylesheet" type="text/css" href="fonts.css">
    <link rel="stylesheet" type="text/css" href="Global.css">
    <script type="text/javascript" src="essentials.js"></script>

    <title>Tesla</title>

</head>
<body>

    <header>
        
        <a id="logo" href="javascript:void(0);"></a>
        
        <div class="heyThereMsg">Hello, <%=name%></div>
                
        <input id="menuCheckbox" class="checkbox" role="button" type="checkbox"/>
        <label class="shadow" for="menuCheckbox"></label>
        
        <label id="burger" for="menuCheckbox">
            <div id="burgerStick-1" class="burgerStick"></div>
            <div id="burgerStick-2" class="burgerStick"></div>
            <div id="burgerStick-3" class="burgerStick"></div>
        </label>


        <div class="menu">
            <div class="menuCarsContainer">
                <a id="ModelS" class="menuItem" href="javascript:void(0);">MODEL S</a>
                <a id="Model3" class="menuItem" href="javascript:void(0);">MODEL 3</a>
                <a id="ModelX" class="menuItem" href="javascript:void(0);">MODEL X</a>
                <a id="ModelY" class="menuItem" href="javascript:void(0);">MODEL Y</a>
                <a id="Admin" class="menuItem" runat="server" href="javascript:void(0);">ADMIN</a>
            </div>
            <form id="logoutForm" method="post"  style="display:none;"> <input id="logoutbutton" name="logoutbutton" class="checkbox" type="submit" onclick="logout"/> </form>
            <div class="logMethods">
                <label id="loginMethod" class="logMethod" runat="server" for="loginPopapCheckbox">LOGIN</label>
                <label id="signupMethod" class="logMethod" runat="server" for="signupPopapCheckbox">SIGN UP</label>
                <label id="logoutMethod" class="logMethod" runat="server" for="logoutbutton">LOGOUT</label>
            </div>
        </div>

    </header>
    

    <div class="popap">
        
        <input id="loginPopapCheckbox" class="checkbox popapCheckbox" role="button" type="checkbox" />
        <label class="shadow" for="loginPopapCheckbox"></label>
        
        <div class="popapContent">
            <div class="hellouser">Login</div>
            <form method="post">
                <input type="text" id="loginUsername" class="username" name="loginUsername" placeholder="Username" spellcheck="false" required oninput="this.setCustomValidity('');" autocomplete="off" oninvalid="this.setCustomValidity('please enter your username')"/><br/>
                <input type="password" id="loginPassword" class="password" name="loginPassword" placeholder="Password" spellcheck="false" required oninput="this.setCustomValidity('');" autocomplete="off" oninvalid="this.setCustomValidity('please enter your password')"/><br/>
                <input id="loginButton" name="loginButton" type="submit" value="LOGIN" />
            </form>
        </div>

    </div>

    <div class="popap">
        <input id="signupPopapCheckbox" class="checkbox popapCheckbox" role="button" type="checkbox"/>
        <label class="shadow" for="signupPopapCheckbox"></label>

        <div class="popapContent">
            <div class="hellouser">Sign Up</div>
            <form method="post">
                <input type="text" id="signupUsername" class="username" name="signupUsername" placeholder="Username" spellcheck="false" required oninput="this.setCustomValidity('');" autocomplete="off" oninvalid="this.setCustomValidity('please enter a username')"/><br/>
                
                <input type="password" id="signupPassword" class="password" name="signupPassword" placeholder="Password" spellcheck="false" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" oninput="updatePassword(this.value); this.setCustomValidity('');" autocomplete="off" oninvalid="this.setCustomValidity('please enter valid password')"/> 
                <div class="passwordValidation">
                    <div class="lowercase">1+ lowercase</div>
                    <div class="uppercase">1+ uppercase</div>
                    <div class="digit">1+ digit</div>
                    <div class="special">1+ special character (!@#$%^&*)</div>
                    <div class="length">8+ total</div>
                </div><br/>
                <input type="password" class="passValidiy" name="signupValidatePassword" style="display:block" placeholder="validate password" spellcheck="false" required oninput="this.setCustonValidity('');" autocomplete="off" oninvalid="this.setCustomValidity('this field does not match the password!')"/>
                <input type="text" id="signupEmail" class="email" name="signupEmail" placeholder="Email" spellcheck="false" required pattern="^[a-zA-Z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$" oninput="this.setCustomValidity('');" autocomplete="off" oninvalid="this.setCustomValidity('please enter valid email')"/><br/>
                <input type="text" id="code" class="code" name="code" placeholder="code" spellcheck="false" oninput="this.setCustomValidity('');" autocomplete="off"/><br/>
                <input id="signupButton" name="signupButton" type="submit" value="SIGN UP" />
            </form>
        </div>
    </div>


    <div class="content">
    </div>
    <div id="userTable"> <%=adminPageContent%> </div>

    <form id="adminModify" method="post" onsubmit="return validateNewValue();">
        <input type="text" id="usernameToModify" class="username" name="usernameToModify" placeholder="username" spellcheck="false" required="required" oninput="this.setCustomValidity('');" autocomplete="off" oninvalid="this.setCustomValidity('please choose a username')" />
        <br/>
        <select name="adminOptions" id="adminOptions" onchange="updateFields(this);">
            <option id="modify" value="modify">modify</option> 
            <option id="delete" value="delete">delete</option> 
        </select>
        <br/>
        <select name="modifyOptions" id="modifyOptions" onchange="updateCurrentOption(this);">
            <option id="modifyEmail" value="email">email</option> 
            <option id="modifyIsAdmin" value="isAdmin">isAdmin 1/0</option> 
        </select>

        <input type="text" id="newValue" name="newValue" placeholder="new value" option="email" spellcheck="false" autocomplete="off" oninvalid="this.setCustomValidity('please enter the new value')"/>
        <br/>
        <input id="adminModifySubmit" name="adminModifySubmit" type="submit" value="enter"/>
    </form>


    <script type="text/javascript" src="all.js"></script>
    
</body>
</html>
