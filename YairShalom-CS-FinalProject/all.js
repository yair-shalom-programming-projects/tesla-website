const MainPage = "Main";
const ModelSPage = "ModelS";
const Model3Page = "Model3";
const ModelXPage = "ModelX";
const ModelYPage = "ModelY";
const AdminPage = "Admin";

const pages = { [MainPage]   : document.getElementById("logo"),
                [ModelSPage] : document.getElementById("ModelS"),
                [Model3Page] : document.getElementById("Model3"),
                [ModelXPage] : document.getElementById("ModelX"),
                [ModelYPage] : document.getElementById("ModelY"),
                [AdminPage] : document.getElementById("Admin") }
var timer = null;
var firstBoot = true;
var menu = document.getElementById("menuCheckbox");


initializeEventListeners();
pages[getCurrentPage()].click();
if (getCurrentPage() == MainPage) window.addEventListener("scroll", enableScrollMovement);



function initializeEventListeners() {    

    let menuItems = document.querySelectorAll(".menuItem");
    
    pages[MainPage].addEventListener("click", ()=>{
        document.getElementById("userTable").style.display = "none"; document.getElementById("adminModify").style.display = "none";
        window.addEventListener("scroll", enableScrollMovement); 
         loadPage(MainPage)
    });


    menuItems.forEach((page) => {
        page.addEventListener("click", ()=>{openMenuPage(page)})
    });


    let html = document.querySelector("html");

    document.querySelectorAll(".checkbox").forEach((checkb) => {
        checkb.addEventListener("change", (checkb)=>{
            checkb.path[0].checked ? 
                html.className += " freezePageScroll" :
                removeClass(html, "freezePageScroll");
    })});


    document.querySelectorAll(".logMethod").forEach((method) => {
        method.addEventListener("click", ()=>{menu.checked = false})
    });

    document.querySelector("#logoutForm").addEventListener("submit", ()=>{pages[MainPage].click()});
}

function enableScrollMovement() {

    let screenHeight = window.innerHeight;
    let currentHeight = window.pageYOffset; 

    if (timer !== null) 
        clearTimeout(timer);        
    
    timer = setTimeout(() => {

        // scroll to first image
        if (currentHeight > 0 &&
            currentHeight < .5 * screenHeight)  
        {
            window.scrollTo({   
                top: 0,
                behavior: 'smooth'
            });
        }
        // scroll to second image
        else if ( currentHeight > .5 * screenHeight &&
                    currentHeight < 1.5 * screenHeight)
        {
            window.scrollTo({   
                top: screenHeight,
                behavior: 'smooth'
            });
        }   
        // scroll to second image
        else if ( currentHeight > 1.5*screenHeight &&
                    currentHeight < 2.5 * screenHeight)
        {
            window.scrollTo({   
                top: 2*screenHeight,
                behavior: 'smooth'
            });
        }          
        // scroll to second image
        else if ( currentHeight > 2.5 * screenHeight &&
                    currentHeight < 3 * screenHeight)
        {
            window.scrollTo({   
                top: 3*screenHeight,
                behavior: 'smooth'
            });
        }       


    }, 50);
}

// page related
function getCurrentPage() {

    var url = window.location.href;

    try { 
        var url = new URL(url);
    } catch(err) {
        console.log(err.message);
    }

    if (url.searchParams == undefined || !url.searchParams.get("page")) 
        return MainPage;
    return url.searchParams.get("page");
}
function setCurrentPage(pageName) {
    window.history.replaceState('object', '', '?page=' + pageName);
}
function loadPage(page) {
        
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = () => {
        if (xmlhttp.readyState == XMLHttpRequest.DONE) { 
        
            if (xmlhttp.status == 200) {

                document.querySelector(".content").innerHTML = xmlhttp.responseText;
                remove_selected_from_menu_items(page);
                setCurrentPage(page);

            } else {
            
                alert('There was an error ' + xmlhttp.status);
                pages[MainPage].click();
            }
        }
    };
    xmlhttp.open("GET", page + '.html', false);
    xmlhttp.send();
}
function remove_selected_from_menu_items(page) {
    let currPage = getCurrentPage();
    if (currPage != page && currPage != MainPage) 
        removeClass(document.getElementById(currPage), "menuItem__selected");
    
}
function openMenuPage(item) {
    let page = getCurrentPage();

    if (page != item.id || firstBoot) 
    {        
        document.getElementById("adminModify").style.display = item.id == AdminPage ? "block" : "none";
        document.getElementById("userTable").style.display = item.id == AdminPage ? "block" : "none";
        item.className += " menuItem__selected";
        firstBoot = false;
    }
    window.removeEventListener("scroll", enableScrollMovement);
    if(menu.checked) menu.click();
    loadPage(item.id);
}

function removeClass(element, classToRemove) {
    element.className = element.className.replaceAll(" "+classToRemove, "");
}

function updateFields(element) {
    let display = element.value == "modify" ? "inline-block" : "none";
    document.getElementById("modifyOptions").style.display = display;
    document.getElementById("newValue").style.display = display;
}

function updateCurrentOption(element) {
    document.getElementById().setAttribute("option", element.value);
}

function validateNewValue() 
{
    if (document.getElementById("adminOptions").value == "modify") 
    {
        let input = document.getElementById("newValue");
        let option = input.getAttribute("option");

        if (option == "email" && !input.value.match(/^[a-zA-Z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/)) {
                alert("please enter a valid email!");
                return false;
        }
        else if (option == "isAdmin" && !input.value.match(/^[0-1]$/)) {
            alert("isAdmin value should be 0 or 1 only!");
            return false;
        }
    }

    return true;
}

