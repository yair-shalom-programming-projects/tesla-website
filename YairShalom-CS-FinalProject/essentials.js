function updatePassword(password) {
    let lower = document.querySelector(".lowercase");
    let upper = document.querySelector(".uppercase");
    let digit = document.querySelector(".digit");
    let special = document.querySelector(".special");
    let length = document.querySelector(".length");

    password.match(/[a-z]/) ? 
        lower.classList.add("valid") :
        lower.classList.remove("valid");
    password.match(/[A-Z]/) ? 
        upper.classList.add("valid") :
        upper.classList.remove("valid");
    password.match(/[\d]/i) ? 
        digit.classList.add("valid") :
        digit.classList.remove("valid");
    password.match(/[!|@|#|$|%|^|&|*]/i) ? 
        special.classList.add("valid") :
        special.classList.remove("valid");
    password.length >= 8 ? 
        length.classList.add("valid") :
        length.classList.remove("valid");
}

function updateVerifyPassword(password) {
    let pass = document.querySelector("#signupPassword");
    console.log(password);
    return password == pass;
}