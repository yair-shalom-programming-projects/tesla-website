﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace YairShalom_CS_FinalProject
{
    public partial class Home : System.Web.UI.Page
    {
        public const string DB = "YairDatabase.mdf";
        public string adminPageContent = "";
        public string name = "";
        
        
        protected void Page_Load(object sender, EventArgs e)
        {
            
            adminPageContent = (string)Session["userstable"];

            if (Session["username"] == null)
            {
                name = "Guest";
                Admin.Style.Add("display", "none");
                logoutMethod.Style.Add("display", "none");
            }
            else
            {
                name = (string)Session["username"];
                Admin.Style.Add("display", Session["admin"] != null && (bool)Session["admin"] != false ? "block" : "none");
                loginMethod.Style.Add("display", "none");
                signupMethod.Style.Add("display", "none");
                logoutMethod.Style.Add("display", "block");
            }


            if (Request.Form["loginButton"] != null)
            {
                string username = Request.Form["loginUsername"];
                string password = Request.Form["loginPassword"];

                string sql = "SELECT * FROM users WHERE username = '" + username + "' AND password = '" + password + "';";
                 

                if (MyAdoHelper.IsExist(DB, sql))
                {
                    Session["username"] = username;
                    name = username;
                    loginMethod.Style.Add("display", "none");
                    signupMethod.Style.Add("display", "none");
                    logoutMethod.Style.Add("display", "block");

                    string isAdmin = "SELECT isAdmin from users WHERE username = '" + username + "';";

                    if (MyAdoHelper.IsAdmin(DB, isAdmin))
                    {
                        Session["admin"] = true;
                        Session["userstable"] = MyAdoHelper.printDataTable(DB, "SELECT username, email, isAdmin FROM users WHERE username != '" + username + "';");
                        adminPageContent = (string)Session["userstable"];
                        Admin.Style.Add("display", "block");
                    }
                    else
                        Session["admin"] = false;
                    
                }
                else
                    Response.Write("<script>alert('user doest not exist!')</script>");
            
            }
            else if (Request.Form["signupButton"] != null)
            {   
                string username = Request.Form["signupUsername"];
                string password = Request.Form["signupPassword"];
                string validatePss = Request.Form["passValidiy"];
                string email = Request.Form["signupEmail"];

                int admin = Request.Form["code"] == "admin1" ? 1 : 0;
                
                string sql = "INSERT INTO users (username, password, email, isAdmin) VALUES ('" + username + "', '" + password + "', '" + email + "', " + admin + ");";


                if (password != validatePss)
                    Response.Write("<script>alert('validate password field should match the password!')</script>");
                
                else if (MyAdoHelper.IsExist(DB, "SELECT * from users WHERE username = '" + username + "';"))
                    Response.Write("<script>alert('username is already exist!')</script>");
                else
                {
                    MyAdoHelper.DoQuery(DB, sql);
                    Session["username"] = username;
                    name = username;
                    Session["admin"] = admin == 1;
                    Session["userstable"] = MyAdoHelper.printDataTable(DB, "SELECT username, email, isAdmin FROM users WHERE username != '" + username + "';");
                    adminPageContent = (string)Session["userstable"];
                    Admin.Style.Add("display", admin == 1 ? "block" : "none");
                    loginMethod.Style.Add("display", "none");
                    signupMethod.Style.Add("display", "none");
                    logoutMethod.Style.Add("display", "block");
                }

            }

            else if (Request.Form["adminModifySubmit"] != null)
            {
                string username = Request.Form["usernameToModify"];
                string option = Request.Form["adminOptions"];

                if (MyAdoHelper.IsExist(DB, "SELECT * FROM users WHERE username = '" + username + "';"))
                {
                    if (option == "delete")
                    {
                        string sql = "DELETE FROM users WHERE username = '" + username + "';";
                        MyAdoHelper.ExecuteNonQuery(DB, sql);
                        Response.Write("<script>alert('user deleted!')</script>");
                    }
                    else if (option == "modify") 
                    {
                        string whatToChange = Request.Form["modifyOptions"];
                        string newVal = Request.Form["newValue"];

                        string sql = "UPDATE users SET " + whatToChange + " = '" + newVal + "' WHERE username = '" + username + "';";
                        MyAdoHelper.ExecuteNonQuery(DB, sql);
                        Response.Write("<script>alert('user updated!')</script>");
                    }

                    Session["userstable"] = MyAdoHelper.printDataTable(DB, "SELECT username, email, isAdmin FROM users WHERE username != '" + name + "';");
                    adminPageContent = (string)Session["userstable"];
                }
                else
                    Response.Write("<script>alert('user doest not exist!')</script>");
            }
            else if (Request.Form["logoutbutton"] != null)
            {
                Session["username"] = null;
                name = "Guest";
                loginMethod.Style.Add("display", "inline-block");
                signupMethod.Style.Add("display", "inline-block");
                logoutMethod.Style.Add("display", "none");
            }

        }
    }
}